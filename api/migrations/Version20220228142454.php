<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220228142454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE autor (id INT AUTO_INCREMENT NOT NULL, nume VARCHAR(50) NOT NULL, prenume VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE marturie (id INT AUTO_INCREMENT NOT NULL, autor_id_id INT NOT NULL, prenume_autor VARCHAR(50) NOT NULL, continut VARCHAR(255) NOT NULL, data DATETIME NOT NULL, INDEX IDX_CBC66E3E7170846C (autor_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mesaj (id INT AUTO_INCREMENT NOT NULL, autor_id_id INT NOT NULL, titlu VARCHAR(50) NOT NULL, data DATETIME NOT NULL, path VARCHAR(255) NOT NULL, INDEX IDX_2C4E47797170846C (autor_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE marturie ADD CONSTRAINT FK_CBC66E3E7170846C FOREIGN KEY (autor_id_id) REFERENCES autor (id)');
        $this->addSql('ALTER TABLE mesaj ADD CONSTRAINT FK_2C4E47797170846C FOREIGN KEY (autor_id_id) REFERENCES autor (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE marturie DROP FOREIGN KEY FK_CBC66E3E7170846C');
        $this->addSql('ALTER TABLE mesaj DROP FOREIGN KEY FK_2C4E47797170846C');
        $this->addSql('DROP TABLE autor');
        $this->addSql('DROP TABLE marturie');
        $this->addSql('DROP TABLE mesaj');
    }
}
