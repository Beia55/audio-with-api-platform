<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MarturieRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MarturieRepository::class)]
#[ApiResource]
class Marturie
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $prenume_autor;

    #[ORM\Column(type: 'string', length: 255)]
    private $continut;

    #[ORM\Column(type: 'datetime')]
    private $data;

    #[ORM\ManyToOne(targetEntity: Autor::class, inversedBy: 'marturies')]
    #[ORM\JoinColumn(nullable: false)]
    private $autorId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenumeAutor(): ?string
    {
        return $this->prenume_autor;
    }

    public function setPrenumeAutor(string $prenume_autor): self
    {
        $this->prenume_autor = $prenume_autor;

        return $this;
    }

    public function getContinut(): ?string
    {
        return $this->continut;
    }

    public function setContinut(string $continut): self
    {
        $this->continut = $continut;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getAutorId(): ?Autor
    {
        return $this->autorId;
    }

    public function setAutorId(?Autor $autorId): self
    {
        $this->autorId = $autorId;

        return $this;
    }
}
