<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AutorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AutorRepository::class)]
#[ApiResource]
class Autor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $nume;

    #[ORM\Column(type: 'string', length: 50)]
    private $prenume;

    #[ORM\OneToMany(mappedBy: 'autorId', targetEntity: Mesaj::class)]
    private $mesages;

    #[ORM\OneToMany(mappedBy: 'autorId', targetEntity: Marturie::class)]
    private $marturies;

    public function __construct()
    {
        $this->mesages = new ArrayCollection();
        $this->marturies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getPrenume(): ?string
    {
        return $this->prenume;
    }

    public function setPrenume(string $prenume): self
    {
        $this->prenume = $prenume;

        return $this;
    }

    /**
     * @return Collection<int, mesaj>
     */
    public function getMesages(): Collection
    {
        return $this->mesages;
    }

    public function addMesage(Mesaj $mesage): self
    {
        if (!$this->mesages->contains($mesage)) {
            $this->mesages[] = $mesage;
            $mesage->setAutorId($this);
        }

        return $this;
    }

    public function removeMesage(Mesaj $mesage): self
    {
        if ($this->mesages->removeElement($mesage)) {
            // set the owning side to null (unless already changed)
            if ($mesage->getAutorId() === $this) {
                $mesage->setAutorId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Marturie>
     */
    public function getMarturies(): Collection
    {
        return $this->marturies;
    }

    public function addMartury(Marturie $martury): self
    {
        if (!$this->marturies->contains($martury)) {
            $this->marturies[] = $martury;
            $martury->setAutorId($this);
        }

        return $this;
    }

    public function removeMartury(Marturie $martury): self
    {
        if ($this->marturies->removeElement($martury)) {
            // set the owning side to null (unless already changed)
            if ($martury->getAutorId() === $this) {
                $martury->setAutorId(null);
            }
        }

        return $this;
    }
}
